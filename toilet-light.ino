#include <TaskScheduler.h>
#include <tinyNeoPixel.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

// === FUNCTION PROTOTYPE ===
void cbEnableLight();
void cbDisableLight();

// === CONSTANTS ===
#define IO_LED_PWR        PB3
#define IO_LED_DATA       PB2
#define IO_INTERRUPT      PB4
#define DURATION_LIGHT_ON 3000
#define PIXEL_NUMBER      24

tinyNeoPixel pixels = tinyNeoPixel(PIXEL_NUMBER, IO_LED_DATA, NEO_GRB + NEO_KHZ800);

bool lightEnabled = false;
Scheduler runner;
Task tEnableLight(0, TASK_ONCE, &cbEnableLight, &runner, false);
Task tDisableLight(0, TASK_ONCE, &cbDisableLight, &runner, false);

void cbEnableLight() {
  lightEnabled = true;
  digitalWrite(IO_LED_PWR, HIGH);
  for (int i=0; i < PIXEL_NUMBER; i++) {
    pixels.setPixelColor(i, pixels.Color(255, 64, 32));
  }
  pixels.show();
}

void cbDisableLight() {
  lightEnabled = false;
  pixels.clear();
  digitalWrite(IO_LED_PWR, LOW);
  digitalWrite(IO_LED_DATA, HIGH);
  sleep();
}

void sleep() {
  ADCSRA &= ~_BV(ADEN);                   // ADC off
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // sleep mode is set here
  sleep_enable();                         // enables the sleep bit in the mcucr register so sleep is possible
  sleep_mode();                           // here the device is actually put to sleep!!
  sleep_disable();                        // first thing after waking from sleep: disable sleep...
  ADCSRA |= _BV(ADEN);                    // ADC on
}

// gets invoked by interrupt when there is a change (falling or rising edge) on the configured interrupt pin
ISR(PCINT0_vect) { 
  if (digitalRead(IO_INTERRUPT) == LOW) {
    tDisableLight.restartDelayed(DURATION_LIGHT_ON);
  } else if (!lightEnabled) {
    tEnableLight.restart();
  }
}

void setup() {
  clock_prescale_set(clock_div_1);
  pinMode(IO_LED_PWR, OUTPUT);
  pinMode(IO_INTERRUPT, INPUT);
  pinMode(IO_LED_DATA, OUTPUT);
  digitalWrite(IO_LED_DATA, HIGH);  // pull high so LED strip doesn't use DATA as GND
  cli();                            // disable interrupts during setup
  GIMSK |= _BV(PCIE);               // pin change interrupt enable
  PCMSK |= _BV(PCINT4);             // pin change interrupt enabled for PCINT4 (PB4)
  sei();                            // enable interrupts
  sleep();                          // start the mcu in sleep mode
}

void loop() {
  runner.execute();
}
